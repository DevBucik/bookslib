package service;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Book;

public class BookServiceTest {

	@Test
	public void testGetUnknownBook() {
		BookService book;
		Book expected;
		Book result;
		
		book = new BookService();
		result = book.getBook("not_allowed_");
		expected = null;
		assertEquals(result, expected);
	}

}
