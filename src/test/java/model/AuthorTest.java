package model;

import static org.junit.Assert.*;

import org.junit.Test;

public class AuthorTest {

	@Test
	public void testAuthroRatingCalc() {
		Author author;
		Double expected;
		
		author = new Author();
		author.addRating(0.0);
		author.addRating(null);
		author.addRating(5.0);
		author.addRating(3.5);
		author.calcAvgRating();
		expected = (0.0 + 5.0 + 3.5) / 3.0;
		assertEquals(expected, author.getAverageRating());
		
		author = new Author();
		author.addRating(null);
		author.addRating(null);
		author.addRating(null);
		author.calcAvgRating();
		expected = 0.0;
		assertEquals(expected, author.getAverageRating());
	}

}
