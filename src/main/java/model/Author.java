package model;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Author {
	
	private final static Logger logger = Logger.getLogger(Author.class.getName());

	@JsonProperty("author")
	String name;
	
	Double averageRating;
	
	private Double ratingSum = 0.0;
	private int booksCount = 0;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}
	public void addRating(Double rating) {
		if (rating != null) {
			this.ratingSum += rating;
			booksCount++;
		}else{
			logger.log(Level.INFO, "Cannot add null rating to author: " + this.name);
		}
	}
	public void calcAvgRating() {
		
		// If author has no book with averageRating param
		// we arbitrarily assign value to 0.0
		// so these authors will appear at the end of the ranking (/api/rating)
		
		if (this.booksCount > 0)
			this.averageRating = this.ratingSum / this.booksCount;
		else
			this.averageRating = 0.0;
	}
	
}
