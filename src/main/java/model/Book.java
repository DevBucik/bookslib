package model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class Book {
	
	String				isbn;
	String 				title;
	String 				subtitle;
	String 				publisher;
	String 				description;
	int					pageCount;
	String				thumbnailUrl;
	String				language;
	String				previewLink;
	Double				averageRating;
	String[]			authors;
	String[]			categories;
	long 				publishedDate;
	
	@JsonProperty("imageLinks")
    private void unpackImageLinks(Map<String,String> imageLinks) {
    	if (imageLinks.containsKey("thumbnail"))
    		this.thumbnailUrl = (String)imageLinks.get("thumbnail");
    }

	@JsonProperty("industryIdentifiers")
    private void unpackIndustryIdentifiers(Map<String,String>[] ids) {
    	for (Map<String,String> i : ids) {
            if (((String)i.get("type")).equals("ISBN_13")) {
            	this.isbn = (String)i.get("identifier");
            	break;
            }
	    }
    }

	@JsonProperty("publishedDate")
    private void parsePublishedDate(String dateString) {
		
		if (dateString.length() <= 4) {
			dateString += "-01-01";
		}
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
		Date date = null;
		try {
			date = dateFormat.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (date != null) {
			this.publishedDate = (long)date.getTime();
		}
    }
	
	public long getPublishedDate() {
		return publishedDate;
    }

	public String[] getAuthors() {
		return authors;
	}
	public void setAuthors(String[] authors) {
		this.authors = authors;
	}
	public String[] getCategories() {
		return categories;
	}
	public void setCategories(String[] categories) {
		this.categories = categories;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getPreviewLink() {
		return previewLink;
	}
	public void setPreviewLink(String previewLink) {
		this.previewLink = previewLink;
	}
	public Double getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}

	
}
