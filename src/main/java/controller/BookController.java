package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.Book;
import service.BookService;

@RestController
@RequestMapping("/api/book")
public class BookController {
	
	@Autowired
	BookService bookService;

	@RequestMapping("{isbn}")
	public Book getBook(@PathVariable("isbn") String isbn) {
		Book book = bookService.getBook(isbn);
		
		if (book == null) throw new BookNotFoundException("No results found");
		
		return book;
	}
	
}
