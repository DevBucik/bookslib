package controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.Book;
import model.Category;
import service.CategoryService;

@RestController
@RequestMapping("/api/category")
public class CategoryController {
	
	@Autowired
	CategoryService categoryService;

	@RequestMapping("{name}/books")
	public ArrayList<Book> getBooks(@PathVariable("name") String name) {
		Category category = categoryService.getCategory(name);
		
		if (category != null)
			if (category.getBooks() != null)
				return category.getBooks();
			else
				return new ArrayList<Book>();
		else
			return new ArrayList<Book>();
	}
	
}
