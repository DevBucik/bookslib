package controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.Author;
import service.AuthorService;

@RestController
@RequestMapping("/api")
public class AuthorController {
	
	@Autowired
	AuthorService authorService;

	@RequestMapping("/rating")
	public ArrayList<Author> getSortedAuthors() {
		return authorService.getSortedAuthors();
	}
	
}
