package service;

import java.util.ArrayList;
import org.springframework.stereotype.Service;

import com.bcsoftware.bookslib.Datasource;

import model.Author;

@Service
public class AuthorService {

	
	public AuthorService() {

	}
	
	public ArrayList<Author> getSortedAuthors() {
		ArrayList<Author> authors = Datasource.getInstance().getAuthors();
		return authors;
	}
	
}
