package service;

import java.util.Hashtable;
import org.springframework.stereotype.Service;

import com.bcsoftware.bookslib.Datasource;

import model.Book;

@Service
public class BookService {

	
	public BookService() {

	}
	
	public Book getBook(String isbn) {
		Hashtable<String, Book> books = Datasource.getInstance().getBooks();
		
		if (books.containsKey(isbn))
			return books.get(isbn);
		else
			return null;
	}
	
}
