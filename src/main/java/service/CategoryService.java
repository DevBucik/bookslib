package service;

import java.util.Hashtable;
import org.springframework.stereotype.Service;

import com.bcsoftware.bookslib.Datasource;

import model.Category;

@Service
public class CategoryService {

	
	public CategoryService() {

	}
	
	public Category getCategory(String name) {
		Hashtable<String, Category> categories = Datasource.getInstance().getCategories();
		
		if (categories.containsKey(name))
			return categories.get(name);
		else
			return null;
	}
	
}
