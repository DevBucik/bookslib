package com.bcsoftware.bookslib;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Hashtable;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.Author;
import model.Book;
import model.Category;

public class Datasource {
	
	Hashtable<String, Book> books = new Hashtable<String, Book>();
	Hashtable<String, Category> categories = new Hashtable<String, Category>();
	ArrayList<Author> authors = new ArrayList<Author>();

    private static Datasource INSTANCE = null;

    private Datasource() {
        if (INSTANCE != null) {
            // SHOUT  
            throw new IllegalStateException("Already instantiated");
        }
    }

	public ArrayList<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(ArrayList<Author> authors) {
		this.authors = authors;
	}

	public static Datasource getInstance() {
        // Creating only  when required.
        if (INSTANCE == null) {
            INSTANCE = new Datasource();
        }
        return INSTANCE;
    }

	public Hashtable<String, Category> getCategories() {
		return categories;
	}

	public void setCategories(Hashtable<String, Category> categories) {
		this.categories = categories;
	}

    public Hashtable<String, Book> getBooks() {
		return books;
	}

	public void setBooks(Hashtable<String, Book> books) {
		this.books = books;
	}
	
	public void readFromJsonFile(String jsonFile) throws IOException {
		
		byte[] encoded = null;
		encoded = Files.readAllBytes(Paths.get(jsonFile));
		String jsonString = new String(encoded, Charset.defaultCharset());

		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = null;
		root = mapper.readTree(jsonString);
		
		if (root != null) {
			JsonNode items = root.get("items");
			if (items != null) {
				
				Hashtable<String, Book> 		books 				= new Hashtable<String, Book>();
				Hashtable<String, Category> 	categories 			= new Hashtable<String, Category>();
				Hashtable<String, Author> 		authors 			= new Hashtable<String, Author>();
				ArrayList<Author> 				authorSortedList 	= new ArrayList<Author>();
				
				for (JsonNode node: items) {
					ObjectMapper objMapper = new ObjectMapper();
					Book b = null;
					b = objMapper.treeToValue(node.get("volumeInfo"), Book.class);
					if (b != null) {
						if (b.getIsbn() != null) {
							books.put(b.getIsbn(), b);
							
						}
						if (b.getCategories() != null) {
							for (String c: b.getCategories()) {
								if (!categories.containsKey(c)) {
									Category category = new Category();
									category.setName(c);
									category.addBook(b);
									categories.put(c, category);
								} else {
									Category category = categories.get(c);
									category.addBook(b);
								}
							}
						}
						if (b.getAuthors() != null) {
							for (String authorName: b.getAuthors()) {
								if (!authors.containsKey(authorName)) {
									Author author = new Author();
									author.setName(authorName);
									author.addRating(b.getAverageRating());
									authors.put(authorName, author);
								} else {
									Author author = authors.get(authorName);
									author.addRating(b.getAverageRating());
								}
							}
							
							authorSortedList.clear();
							for (String authorName: authors.keySet()) {
								Author author = authors.get(authorName);
								author.calcAvgRating();
								authorSortedList.add(author);
							}
							
							authorSortedList.sort((o1, o2) -> Double.compare(o2.getAverageRating(), o1.getAverageRating()));
						}
					}
				}
				
				this.books = books;
				this.categories = categories;
				this.authors = authorSortedList;
			}
		}
	}

}
