package com.bcsoftware.bookslib;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan({"controller","service"})
@SpringBootApplication
public class BookslibApplication {
	
	private final static Logger logger = Logger.getLogger(Datasource.class.getName());	

	public static void main(String[] args) {
		
		String jsonFile = null;
		
		for (int i=0; i<args.length; i++) {
			if (args[i].equals("-source")) {
				if (i+1<args.length) {
					jsonFile = args[i+1];
					break;
				}
			}
		}
		
		if (jsonFile == null) {
			logger.log(Level.SEVERE, "Miss -source argument (datasource filepath)");
		}else {
			try {
				Datasource.getInstance().readFromJsonFile(jsonFile);
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Can't find datasource file '" + jsonFile + "' or bad JSON format");
				return;
			}
			SpringApplication.run(BookslibApplication.class, args);
		}
		
	}
	
}
